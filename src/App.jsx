import React, { Component } from "react";

class App extends Component {
  state = {
    count: 0,
  };

  componentDidMount() {
    document.title = `${this.state.count}`;
  }

  componentDidUpdate(prevState) {
    if (prevState.count !== this.state.count) {
      document.title = `${this.state.count}`;
    }
  }

  add = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  substract = () => {
    this.setState({
      count: this.state.count - 1,
    });
  };

  render() {
    return (
      <div>
        <Counter count={this.state.count} />
        <ButtonCounter add={this.add} substract={this.substract} />
      </div>
    );
  }
}

class Counter extends Component {
  render() {
    return (
      <div>
        <h3>Counter is {this.props.count}</h3>
      </div>
    );
  }
}

class ButtonCounter extends Component {
  render() {
    return (
      <div>
        <button onClick={this.props.add}>+</button>
        <button onClick={this.props.substract}>-</button>
      </div>
    );
  }
}

export default App;
